let navItems = document.querySelector("#navSession");
let isAdmin = localStorage.getItem("isAdmin");
let userToken = localStorage.getItem("token");
let userGreeting = document.querySelector("#userGreeting");

if(!userToken) {
	navItems.innerHTML +=
	`
		<li class="nav-item">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	if (isAdmin == "true"){
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
	`
	} else {
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
		
	`	
	}
}

if (!userToken) {
	userGreeting.innerHTML += `Hello, Guest!`;
} else {
	if (isAdmin === "true") {
		userGreeting.innerHTML += "Hello, Admin!";
	} else {
		userGreeting.innerHTML += "Hello, User!";
	}
}

