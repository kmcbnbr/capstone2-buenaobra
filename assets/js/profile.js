let params = new URLSearchParams(window.location.search);
let courses = document.querySelector("#coursesEnrolled");
let userCard = document.querySelector("#userCard");

fetch(`https://secret-badlands-66756.herokuapp.com/api/users/details/`, {
	method: "GET",
	headers: {
		Authorization: `Bearer ${userToken}`,
	},
})
	.then((res) => res.json())
	.then((user) => {
		userCard.innerHTML = `
			<img class="avatar rounded-circle mx-auto" src="../assets/images/user.png">
			<div class="text-secondary mt-2">Name</div>
			<h2>${user.firstName} ${user.lastName}</h2>
			<div class="text-secondary">Email</div>
			<p class="text-primary">${user.email}</p>
			<div class="text-secondary">Mobile Number</div>
			<p class="text-primary mb-0">${user.mobileNo}</p>
		`

		fetch("https://secret-badlands-66756.herokuapp.com/api/courses/all", {
			method: "GET",
			headers: {
				Authorization: `Bearer ${userToken}`
			}
		})
			.then((res) => res.json())
			.then((coursesData) => {
				user.enrollments.forEach((enrollment) => {
					let course = coursesData.find(
						(course) => enrollment.courseId == course._id
					);
					courses.innerHTML += `<li class="my-2">${course.name}</li>
					`;
				});
			});
	});
