let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let addEnrollees = document.querySelector("#enrollees");
let enrolledLabel = document.querySelector("#enrolledLabel")

fetch(`https://secret-badlands-66756.herokuapp.com/api/courses/${courseId}`)
	.then((res) => res.json())
	.then((data) => {
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`;
		document
			.querySelector("#enrollButton")
			.addEventListener("click", () => {
				fetch("https://secret-badlands-66756.herokuapp.com/api/users/enroll", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${userToken}`,
					},
					body: JSON.stringify({
						courseId: courseId,
					}),
				})
					.then((res) => res.json())
					.then((data) => {
						if (data) {
							alert("You have enrolled successfully.");
							window.location.replace("./courses.html");
						} else {
							alert("Enrollment Failed");
						}
					});
			});

		if (isAdmin == "true") {
			enrolledLabel.innerHTML = "Students Enrolled"
			fetch("https://secret-badlands-66756.herokuapp.com/api/users/", {
				method: "GET",
				headers: {
					Authorization: `Bearer ${userToken}`,
				},
			})
				.then((res) => res.json())
				.then((usersData) => {
					data.enrollees.forEach((enrollee) => {
						let user = usersData.find(
							(user) => enrollee.userId == user._id
						);
						addEnrollees.innerHTML += `<li class="my-2">${user.firstName} ${user.lastName}</li>`;
					});
				});
		}
	});
