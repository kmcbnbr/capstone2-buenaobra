let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	if (email == "" || password == "") {
		alert("Please input your email/password");
	} else {

		fetch("https://secret-badlands-66756.herokuapp.com/api/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if(data !== false && data.access !==null){
					localStorage.setItem('token', data.accessToken)
					fetch('https://secret-badlands-66756.herokuapp.com/api/users/details', {
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}
					})
					.then(res => res.json())
					.then(data => {
						localStorage.setItem('id', data._id)
						localStorage.setItem('isAdmin', data.isAdmin)
						localStorage.setItem('courses', JSON.stringify(data.enrollments))
						window.location.replace('./courses.html')

					})
				} else {
					alert('Login Failed. Something went wrong')
				}
							
				})
	}
});
