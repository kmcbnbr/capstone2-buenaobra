let registerForm = document.querySelector("#registerForm");

registerForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if (
		password1 !== "" &&
		password2 !== "" &&
		password1 === password2 &&
		mobileNumber.length === 11
	) {
		fetch("https://secret-badlands-66756.herokuapp.com/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === false) {
					fetch("https://secret-badlands-66756.herokuapp.com/api/users/", {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNumber,
						}),
					})
						.then((res) => res.json())
						.then((data) => {
							if (data === true) {
								alert("Registration Successful");
								window.location.replace("./login.html")
							} else {
								alert("Registration Failed");
							}
						});
				} else {
					alert("Email Already Exist");
				}
			});
	} else {
		alert("Invalid Input");
	}
});
