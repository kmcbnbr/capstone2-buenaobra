let addButton = document.querySelector("#adminButton");
let urlAdmin = "https://secret-badlands-66756.herokuapp.com/api/courses/all";
let urlUser = "https://secret-badlands-66756.herokuapp.com/api/courses/";

if (isAdmin == "true") {
	addButton.innerHTML += ` 
		<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary"> Admin </a></div>
	`;
} else {
	addButton.innerHTML = null;
}

fetch(isAdmin == "true" ? urlAdmin : urlUser, {
	method: "GET",
	headers: {
		Authorization: `Bearer ${userToken}`,
	},
})
	.then((res) => res.json())
	.then((data) => {
		if (data.length < 1) {
			courseData = "No Courses Available";
		} else {
			courseData = data
				.map((course) => {
					let cardFooter;
					if (isAdmin !== "true") {
						cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go to Course</a>`;
					} else {
						let adminCourseBtn = course.isActive == true
							? `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-warning text-white btn-block">Archive Course</a>`
							: `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>`

						cardFooter = `
							<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go to Course</a>
							${adminCourseBtn}
						`;
					}
					return `
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`;
				})
				.join("");
		}
		let container = document.querySelector("#coursesContainer");
		container.innerHTML = courseData;
	});
