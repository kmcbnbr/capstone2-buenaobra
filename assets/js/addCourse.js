let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault();
	let courseName = document.querySelector("#courseName").value;
	let description = document.querySelector("#courseDescription").value;
	let price = document.querySelector("#coursePrice").value;

	let token = localStorage.getItem("token");

	fetch("https://secret-badlands-66756.herokuapp.com/api/courses/", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`,
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price,
		}),
	})
		.then((res) => res.json())
		.then((data) => {
			if(data){
				window.location.replace("./courses.html")
			} else {
				alert("Something went wrong. Course not added.")
			}

		});
});
