let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

let token = localStorage.getItem("token");
fetch(`https://secret-badlands-66756.herokuapp.com/api/courses/activate/${courseId}`, {
	method: "PUT",
	headers: {
		Authorization: `Bearer ${token}`,
	},
})
	.then(res => res.json())
	.then(data => {
		if(data){
			alert("Course Activated.")
			window.location.replace("./courses.html")
		} else {
			alert("Something Went Wrong.")
		}
	});
