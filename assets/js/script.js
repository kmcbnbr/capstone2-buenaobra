let navItems = document.querySelector("#navSession");
let isAdmin = localStorage.getItem("isAdmin");
let userToken = localStorage.getItem("token");

if (!userToken) {
	navItems.innerHTML +=
		`
		<li class="nav-item${window.location.pathname.includes("login") ? " active" : ""}">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item${window.location.pathname.includes("register") ? " active" : ""}">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	if (isAdmin == "true") {
		navItems.innerHTML +=
			`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	`
	} else {
		navItems.innerHTML +=
			`
		<li class="nav-item${window.location.pathname.includes("profile") ? " active" : ""}">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
		
	`
	}
}
